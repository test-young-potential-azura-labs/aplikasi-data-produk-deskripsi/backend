const { Product } = require('../models');
const Validator = require('fastest-validator');
const v = new Validator();
module.exports = {
  index: async (req, res) => {
    try {
      const product = await Product.findAll();
      res.status(200).json(product);
    } catch (err) {
      res.respondServerError(err.message);
    }
  },

  show: async (req, res) => {
    try {
      const product_id = req.params.id;
      const check = await Product.findOne({ where: { id: product_id } });
      if (!check) return res.respondNotFound();

      const product = await Product.findOne({
        where: {
          id: product_id,
        },
      });
      res.status(200).json(product);
    } catch (err) {
      res.respondServerError(err.message);
    }
  },
  create: async (req, res) => {
    try {
      const schema = {
        title: 'string|min:4|max:255',
        // price: 'number|opsional',
      };

      const validate = v.validate(req.body, schema);
      if (validate.length) return res.respondBadRequest(validate);

      const { title, price } = req.body;
      const product = await Product.create({
        title,
        price,
      });
      // res.status(201).json(product);
      res.respondCreated(product);
    } catch (err) {
      res.respondServerError(err.message);
    }
  },

  update: async (req, res) => {
    try {
      // const product_id = req.params.id;
      // const query = { where: { id: product_id } };

      // const check = await Product.findOne({ where: { id: product_id } });
      // if (!check) return res.respondNotFound();

      const schema = {
        title: 'string|min:4|max:255',
        // price: 'number|min:1|opsional',
      };

      const validate = v.validate(req.body, schema);
      if (validate.length) return res.respondBadRequest(validate);

      // const { title, price } = req.body;
      // const user = await Product.update(
      //   {
      //     title,
      //     price,
      //   },
      //   query
      // );

      const product = await Product.update(req.body, {
        where: { id: req.params.id },
      });
      res.respondUpdated(product);
    } catch (err) {
      res.respondServerError(err.message);
    }
  },

  destroy: async (req, res) => {
    try {
      const product_id = req.params.id;
      const check = await Product.findOne({ where: { id: product_id } });
      if (!check) return res.respondNotFound();

      const product = await Product.destroy({ where: { id: product_id } });
      res.respondDeleted(product);
    } catch (err) {
      res.respondServerError(err.message);
    }
  },
};
