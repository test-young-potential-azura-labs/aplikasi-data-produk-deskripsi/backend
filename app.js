require('dotenv').config();
const express = require('express');
// const morgan = require('morgan');
const cors = require('cors');
const app = express();
const helper = require('./helpers/response');
const router = require('./route');
const { PORT } = process.env;

// app.use(morgan('dev'));
app.use(express.json());
app.use(helper);
app.use(cors());
app.use(`${process.env.BASEURL}`, router);

app.listen(PORT, () => {
  console.log(`App running on port ${PORT}`);
});
