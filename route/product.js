const express = require('express');
const router = express.Router();
const product = require('../controllers/productController');
router.get('/', product.index);
router.get('/:id', product.show);
router.post('/', product.create);
router.patch('/:id', product.update);
router.delete('/:id', product.destroy);

module.exports = router;
